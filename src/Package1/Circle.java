package Package1;

public class Circle extends Shape{

    private double length;

    public Circle(double length) {
        this.length = length;
    }

    @Override
    public double getArea(){
        return Math.PI * (length * length);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

}
