package Package1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1);
        Circle circle2 = new Circle(2);
        Rectangle rectangle1 = new Rectangle(3,4);
        Rectangle rectangle2 = new Rectangle(5,6);
        Square square1 = new Square(7);
        Square square2 = new Square(8);


        List<Shape> shapes = new ArrayList<>();
        addToList(shapes,rectangle1,circle2);
        System.out.println("Площадь фигур = " + getArea(shapes));


        List<Circle> circles = new ArrayList<>();
        addToList(circles,circle1,circle2);
        System.out.println("Площадь кругов = " + getArea(circles));

        List<Rectangle> rectangles = new ArrayList<>();
        addToList(rectangles,rectangle1,rectangle2);
        System.out.println("Площадь прямоугольников = " + getArea(rectangles));

        List<Square> squares = new ArrayList<>();
        addToList(squares,square2,square1);
        System.out.println("Площадь квадратов = " + getArea(squares));

    }

    @SafeVarargs
    public static <T extends Shape> void addToList(List<T> items, T...shapes){
        items.addAll(Arrays.asList(shapes));
    }

    public static double getArea(List<? extends Shape> items){
        double result = 0;
        for (Shape shape : items){
            result += shape.getArea();
        }
        return result;
    }


}
